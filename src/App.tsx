import React, { useState, useRef } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";

interface ITask {
  name: string;
  done: boolean;
}

const App: React.FC = () => {
  const [newTask, setNewTask] = useState<string>("");
  const [tasks, setTasks] = useState<ITask[]>([]);
  const taskInput = useRef<TextInput>(null);

  const addTask = (name: string): void => {
    const newTasks: ITask[] = [...tasks, { name, done: false }];
    setTasks(newTasks);
  };

  const toggleDoneTask = (i: number): void => {
    const newTasks: ITask[] = [...tasks];
    newTasks[i].done = !newTasks[i].done;
    setTasks(newTasks);
  };

  const removeTask = (i: number): void => {
    const newTasks: ITask[] = [...tasks];
    newTasks.splice(i, 1);
    setTasks(newTasks);
  };

  const handleSubmit = () => {
    if (newTask.trim() !== "") {
      addTask(newTask);
      setNewTask("");
      taskInput.current?.clear(); // For TextInput in React Native
      taskInput.current?.focus();
    }
  };

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <TextInput
        style={{ height: 40, borderColor: "gray", borderWidth: 1, width: 200 }}
        onChangeText={(text: React.SetStateAction<string>) => setNewTask(text)}
        value={newTask}
        ref={taskInput}
        autoFocus
      />
      <TouchableOpacity onPress={handleSubmit} style={{ marginTop: 10 }}>
        <Text style={{ color: "green" }}>Save</Text>
      </TouchableOpacity>

      {tasks.map((t: ITask, i: number) => (
        <View key={i} style={{ marginTop: 10 }}>
          <Text style={{ textDecorationLine: t.done ? "line-through" : "none" }}>
            {t.name}
          </Text>
          <View style={{ flexDirection: "row", marginTop: 5 }}>
            <TouchableOpacity onPress={() => toggleDoneTask(i)}>
              <Text style={{ color: "blue" }}>{t.done ? "✓" : "✗"}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => removeTask(i)} style={{ marginLeft: 10 }}>
              <Text style={{ color: "red" }}>🗑</Text>
            </TouchableOpacity>
          </View>
        </View>
      ))}
    </View>
  );
};

export default App;
